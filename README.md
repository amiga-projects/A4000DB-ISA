# Amiga 4000 Desktop Daughterboard Replacement

## Problem

While the original daughterboard for the Amiga 4000 Desktop computer is working
fine, there's still room for improvement:

 * The Zorro-bus is passively terminated by voltage dividers that don't quite
   fit the impedance of the relatively short daughterboard traces. In addition,
   the passive termination over resistor networks introduces a voltage drop on
   the +5V rail and produces heat.
   For an in-depth discussion, cf. https://www.a1k.org/forum/index.php?threads/90216/
 * Aging throughhole components of the original daughterboards and in the 
   original power supplies are difficult to replace and contribute further to
   voltage drop and unreliable operation.

## Intended Solution

This project aims to address the above issues by various improvements over the
original design:

 * Here, the Zorro-bus gets **actively terminated** by two LT1117xxx LDO
   regulators that aim to provide a stable termination voltage of ~2.85V over
   180 Ohms.  While 180 Ohms is a little higher than the (theoretical) impedance
   of ~110...120 Ohms, it is a tradeoff of the capabilities of bus driving
   current between the components commonly seen on Zorro 2 vs. Zorro 3 cards.
 * Two **ATX-header** footprints to choose from allow to power the Amiga 4000D
   by connecting a modern ATX/SFX power supply directly to the daughterboard.
   This way, power is delivered right to the center of the mainboard, with
   measured voltage drop to the outer regions of around ~0.05V
 * The popular **CPLDICY** to connect I²C devices to the Amiga **is integrated**
   into the daughtherboard as a "virtual" Zorro card. One of the two I²C
   connectors is placed near the CPU slot to easily read out values from, e.g.,
   the popular [BFG9060 cpu board](https://gitlab.com/MHeinrichs/a4000-tk),
   created by Matthias Heinrichs.
   Also included are a voltage/temperature monitoring device (LTC2990) and a
   PWM fan controller (MAX31760) that can be controlled by Henryk Richters
   [fanny](https://gitlab.com/HenrykRichter/i2csensors) tool.
 * The parallel port's <tt>SEL</tt> signal is broken out to allow connection of
   a *Ratte monitor switch*, developed by André 'Ratte' Pfeiffer or a similar
   solution.

Current release is **0.6.5 ISA**:

![A4000DB ISA (front view)](images/A4000DB_0.6.5_ISA_front.png)
![A4000DB ISA (rear view)](images/A4000DB_0.6.5_ISA_back.png)

## Attribution

Many of the ideas outlined above weren't developed by me. In fact, this project
would never have been possible without the work of several knowledgable and
inventive people. Here's a (probably incomplete) list of attributions:

 * **Matthias 'Matze' Heinrichs** and **Henryk 'buggs' Richter** originally
   created a daughterboard for the Amiga AA3000 prototype, and later for the
   Amiga 3000D that incorporates active Zorro bus termination and an ICY
   controller.
 * **Matthias 'Berserker_Mat' Knaupe** adapted this design to the Amiga 4000D.
 * User **dorken** on a1k.org initiated a rich discussion about how to properly
   terminate the Zorro bus.
 * **buggs** also developed the CPLDICY that includes the MAX31760 PWM fan
   controller and programmed the necessary software to use I²C devices in an
   Amiga.

I took **Berserker_Mat's** design and:

 * cleaned up the KiCAD schematics and made it into split sheets,
 * replaced, wherever possible, the throughhole components by SMD parts and
   combined them into arrays,
 * refined the Zorro bus termination,
 * added the ATX power connectors and surrounding circuitry,
 * added a two-channel PWM fan controller,
 * added the possibility to choose between 3.3V and 5V power supply for both
   external I²C ports independantly,
 * slightly adjusted the placement and size of slots, connectors, and other
   components to better fit the measurements I did on my original A4000D Rev. B
   daughterboard,
 * added pin headers for 5V and 12V to have these voltages readily available
   inside the Amiga 4000D.
 * added a pin header that provides +5V, GND, and the SEL signal from the
   parallel port to control an automatic monitor switch via **André 'Ratte'
   Pfeiffer's** [switchcontrol](https://aminet.net/package/driver/moni/switchcontrol)
 
Special thanks go to the numerous testers who gave a lot of hints for
improvements!

This repository will include all necessary files to build an Amiga 4000 Desktop
Daughterboard Replacement with the above features. For details, see the section
'Build Instructions' below.

## License and Disclaimer

**BEFORE** going ahead and building one or more of these daughterboards, please
consider the following:

***This project is shared with you under a Creative Commons Attribution-ShareAlike 4.0
license.***
You may produce, distribute, and use this project freely ***as long as you***:
 * Leave **all attributions and copyright notices** which are present in this
   repository, within the design files, and on the final product (silkscreen)
   intact, and only add to them
 * **Share** any modifications and improvements under the same license
 * **Indicate** that you modified the product and what modifications you made
 
The exact license terms can be found in the [LICENSE](LICENSE) file.

***DISCLAIMER***

This is a hobbyist project.

Although it works for me, there's no guarantee that it will work for you, your
neighbor, or your buddy.

Use it at your own risk!

I, TORSTEN KURBAD, EXPRESSLY DISCLAIM ANY RESPONSIBILITY FOR ANY DAMAGE, INJURY,
HARM, COST, EXPENSE, OR LIABILITY ARISING OUT OF OR RELATED TO YOUR USE OR MISUSE
OF THE INFORMATION IN THIS REPOSITORY. THE INFORMATION IS PROVIDED ON AN AS-IS
BASIS AND WITHOUT WARRANTY OF ANY KIND, WHETHER EXPRESS, IMPLIED, OR STATUTORY,
INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PUPOSE, TITLE, ACCURACY, NON-INFRINGEMENT, OR QUALITY.

All Trademarks used are the property of their legal owners and are used solely
for descriptive purposes herein.

## User manual

Here's an overview of the board:

![A4000DB ISA (front view)](images/A4000DB_0.6.5_ISA_front_legend.png)
![A4000DB ISA (rear view)](images/A4000DB_0.6.5_ISA_back_legend.png)

The marked elements provide the following functions.

### Front side

 * **&#10102; I²C connector #1** - here you can connect external I²C devices
 * **&#10102;.&#10102; I²C voltage #1** - put a jumper between middle pin and
   3.3V pin or middle pin and 5V pin to feed the corresponding voltage to the
   I²C connector's '+' pin. Leave open to provide no power at all to the I²C
   connector.
 * **&#10103; 12V header** - this breaks out the +12V rail for measurements or
   to power arbitrary stuff.
 * **&#10104; 5V header** - same as above, but for +5V.
 * **&#10105; JTAG header** - this is to program the ICY's CPLD with its
   [Jedec file](logic/IcyA3000.jed). The connector has the standard Xilinx
   pinout.
 * **&#10106; External temp sensor** - here, you can wire up a 2N3906
   transistor as an external temperature probe for the fan controller. You can
   place the transistor in a hotspot to drive your fans based on correct
   temperature measurements. Connect the transistor's emitter to '+', collector
   and base to '-'.
 * **&#10107; Fan #1**, **&#10108; Fan #2** - these are connectors for 2 pin,
   3 pin, or 4 pin 12V fans. 2 pin fans will always run at a static 12V, 3
   pin fans will provide readouts of their tachometers. These can be displayed
   with **buggs'** excellent [i2c utilities](https://gitlab.com/HenrykRichter/i2csensors).
   In addition to that, the speed of 4 pin fans can be controlled by PWM
   depending on temperature readouts. The software for that is 'fanny' from
   **buggs'** software package.
 * **&#10109; Disable ICY** - place a jumper here to disable the I²C controller
   and sensor devices. This can be useful if the *ICY* interferes with cards
   on the Zorro bus.
 * **&#10110; Power-switch selector** - if you want to power your Amiga from
   one of the **ATX connectors** on the back side of the board, you have to
   select what type of switch you want to use to turn on and off the power.
   Put a jumper between middle pin and 'Switch' to use a latching switch or
   between middle pin and 'Push button' to use a momentary push button.
   Be careful when using the latter: There's no delay when powering off.
   **PLEASE, READ THE SECTION 'ATX Power' BELOW!!!**

   **IMPORTANT NOTE! If you received one of the Rev. 0.5 ISA boards from me,**
   **you will notice that 'Switch' and 'Push button' are marked the wrong way**
   **round on the board! Use the 'Switch' position for momentary push buttons**
   **and vice versa. This has been corrected in the latest release.**
 * **&#10111; Monitor switch connector** - here, you can connect a 'Ratte
   monitor switch' or similar. Note that this is only the connection necessary
   to do the actual switching. You still have to connect the video inputs to
   your switch. **This header is not present on boards prior to Rev. 0.6.5.**

### Back side

 * **&#10112; ATX connector #1**, **&#10115; ATX connector #2** - you can
   power your Amiga 4000D by a common ATX or SFX power supply using these
   connectors. **IMPORTANT! READ THE SECTION 'ATX Power' BELOW!!!**
 * **&#10113; I²C connector #2** - second I²C connector. For a description, read
   **&#10102; I²C connector #1** in the 'Front side' section. This connector is
   placed here to conveniently connect to a BFG9060 accelerator card.
 * **&#10113;.&#10113; I²C voltage #2** - second I2C voltage selection. For a
   description, read **&#10102;.&#10102; I²C voltage #1** in the 'Front side'
   section.
 * **&#10114; /FAIL signal connector** - when powering your machine by ATX,
   this **must** be wired to pin #1 of the mainboard power connector.
   **SEE THE SECTION 'ATX Power' FOR DETAILS!**
 * **&#10116; Power switch connector** - this is to connect an external switch
   or push button to turn system power on and off when powering the machine by
   ATX.
   **Heads up! On the Rev. 0.5 ISA boards some of you received from me, both**
   **the '/FAIL signal connector' and the 'Power switch connector' are at the**
   **top of the board, near the 'ATX connector #1'.**

### ATX Power

The daughterboard allows you to power your Amiga 4000D with an ATX power supply
or a smaller derivative, like SFX.

This has some advantages:

 * You can use a modern high-class power supply with low noise fan and solid
   capacitors.
 * There's no need for an additional adapter, and power is "injected" right at
   the center of the mainboard. Therefore, the voltage drop is very low and the
   power supply operates with its intended cable length. This helps a lot with
   proper power regulation.
 * You can potentially feed more Watts into the system than the original power
   supply would be able to provide, helping with power-hungry accelerators and
   plug-in cards.
 
**But there are several important things to consider to keep your machine safe:**

 * **Only use power supplies from well known manufacturers!** Spend some money
   to keep your precious Amiga safe! Don't go Noname Chinese brand!
 * **Use power supplies that provide between 300 and 400W!** You can get into
   big trouble with a 600 ~ 1000W class ATX PSU. These have lots of power on
   their 12V rails, and all regulation is based on the load there. Next to
   nothing uses 12V in the Amiga, so there will be a huge imbalance between
   12V and 5V, which can result in everything from not working at all to
   destroying your Amiga! **YOU HAVE BEEN WARNED!**
 * **NEVER!!!**, I repeat **NEVER!!!** power your Amiga from **MORE THAN ONE**
   **POWER SUPPLY!!!** Choose the **ONE** from the two ATX connectors that suits
   you best and leave the other unconnected **OR** use a traditional power
   supply connected to the **mainboard**.
   **AGAIN; YOU HAVE BEEN WARNED!**

To give a hint for the class of PSU you should be after, my personal
recommendation is the 'be quiet! BQT SFX-300W.' Note that I'm not affiliated
with this company in any way.

If you choose to use an ATX power supply, you have to connect it as follows:

 * Connect the 24 pin ATX connector to **&#10112; ATX connector #1** or
   **&#10115; ATX connector #2** depending on your preference.
 * Connect a wire between **&#10114; /FAIL signal connector** and **Pin #1** of
   the mainboard power connector. If you received a board from me, I provided
   a cable for that. If you need to make one yourself, s. 'Build Instructions'
   below.
 * Connect a **latching switch** (rated at least 5V, 100mA) or a **momentary**
   **push button** to the **&#10116; Power switch connector**.
 * Put a jumper in the appropriate position on **&#10110; Power-switch selector**
   header depending on what switch type you chose.

Here are some pictures to demonstrate the 'ATX' and '/FAIL' connections.
Note that this shows a Rev. 0.5 board, where the connections are next to the
upper ATX connector:

![Overview](images/A4000DB_ATX_1.png)
![FAIL](images/A4000DB_ATX_2.png)
![Mainboard](images/A4000DB_ATX_3.png)

Once you connected everything up properly, you should be able to power on your
Amiga by flipping or pushing the switch connected to 'Power switch connector'.

## Build Instructions

This is not a beginner project! To build this daughterboard, you need a good
soldering iron and magnification.

First, have PCBs produced by your favourite prototyping manufacturer. The
*gerber* archive to do so is in the <tt>jlcpcb/production_files</tt> folder.

Make sure to select the following features:

 * ENIG - you definitely want gold finish on a plugin card
 * Gold fingers - this will add an extra hard gold layer to the connector
 * Chamfer - this will make it easier to plug the board in
 * Remove order number: 'Specify a location' (JLCPCB only) - This will move the
   number they add to silk screen under one of the ISA slots
   
If you choose [JLCPCB](https://jlcpcb.com) as your manufacturer, I also highly
recommend that you tick

 * Confirm production file
 
If you let them populate the boards (s. bottom for details), I also recommend 
sending the following picture to their support after you ordered.

![A4000DB (front view)](images/JLCPCB_paneling.jpg)

They tend to add their production rails to the longer sides of the board, which
in turn makes *gold fingers* and *chamfer* impossible.

For people who like to solder themselves, the
[HTML BOM](https://gitlab.com/amiga-projects/A4000DB-ISA/-/blob/main/bom/ibom.html?ref_type=heads)
will be of great help.
**Important!** In case you want to power your Amiga via one of the **ATX
connectors** on the daughterboard, you also need:
 * 1 MOLEX Minifit JR 6 pin (TE Connectivity part #350715-4)
 * 1 crimp terminal (TE Connectivity part #350218-1)
 * 1 single dupont wire (~15 cm for board revision 0.6.5 ISA, ~30 cm for older
   revisions)
With the help of these items you have to build a connection between the
<tt>/FAIL</tt> pin on the daughterboard and pin 1 of the mainboard power
connector.

While building the daughterboard, the following features are *optional*:

 * **ATX connectors** (J4, J5) - you can populate both, only one of them, or none.
   If you **don't populate** the connectors **at all**, you can also leave out:
   * JP4, JP5
   * J6
   * IC7, IC8
   * C70, C71, C72, C73, C80
   * R71, R72, R73
 * **Power headers** - you can leave out J7 and/or J8
 * **Monitor switch header** - you can leave out D1 and J9
 * **Fan headers** - you can leave out J1 and/or J2
 * **CPLDICY** - if you don't need the onboard *CPLDICY* I²C controller, you
   **have to** close SJ1. Then you can leave the following parts unpopulated:
   * I2C1, I2C2
   * F1, F2
   * J3
   * JP1, JP2, JP3
   * IC3, IC4, IC5, IC6
   * Q50
   * C30, C31, C32, C33, C40, C50, C51, C60, C61, CX1
   * R30, R40, R41, R42, R43, R50, R51, R52, R53
   * RN60, RN61
   * X1
   * The **fan headers** (J1, J2) will be fixed to 12V in this setting. Of
      course, you can leave them unpopulated if you wish.
 * **Integrated I²C devices** - if you choose to populate the **CPLDICY** you
   can still decide to not populate different aspects of it:
   * **Headers** - you can leave out the external headers, in particular
     * I2C1, F1, C1, JP2 *and/or*
     * I2C2, F2, C11, JP3
   * **Fan control** - if you don't need the PWM fan controller, you don't need
      to populate IC6, C60, C61, RN60, and RN61.
      Note that the **fan headers** (J1, J2) will be fixed to 12V then. Of
      course, you can leave them unpopulated if you wish.
   * **Temperature and voltage control** if you don't need the integrated voltage
      and temperature monitoring, you can leave J3, IC5, Q50, C50, C51, R50,
      R51, R52, and R53 unpopulated

Begin by populating all SMD components on the riser PCB. Start with the small
ones, i.e. the resistors, then ceramic capacitors, fuses, ferrites, ...
Of the SMD components, solder the electrolytic capacitors last.

After a thorough final inspection, plug the daughterboard carefully into the
connectors CN600 and CN601 on your Amiga 4000D mainboard.

Do a brief test **without** any Zorro cards first! If everything seems to work
nicely, one by one test features like **ATX power**, the **video slot**, and
the **Zorro slots**.

Alternatively, you can have JLCPCB solder almost all components for you through
their PCBA service. The folder <tt>jlcpcb/production_files</tt> includes the
necessary *gerber*, *BOM*, and *CPL* files.

Have fun!

## Copyright

This work and documentation is © 2024 by Torsten 'torsti76' Kurbad
